// Achtung: Entgegen der Angaben vom Arbeitsblatt werden hier die Aufgaben "einfach" der
// Reihenfolge nach benannt
public class BedingteWiederholungen{
    // Attribute
    // Keine Attribute
    // Attribute Ende
    // ----------------------------------------
    // Konstruktoren
    public BedingteWiederholungen(){
        // Leerer Konstruktor
    }
    // Konstruktoren Ende
    // ----------------------------------------
    // Methoden
    public int aufgabe1(double anfangskapitalPar, double endkapitalPar, double zinssatzPar){
        int anzahlLaufjahreLok = 0;
        double aktuelleKapitalLok = anfangskapitalPar;
        while(aktuelleKapitalLok <  endkapitalPar){
            anzahlLaufjahreLok++; // gleichbedeutend mit anzahlLaufjahrePar = anzahlLaufjahrePar + 1;
            aktuelleKapitalLok = aktuelleKapitalLok * (1 + zinssatzPar);
        }
        System.out.println("Damit das Anfangskapital " + anfangskapitalPar +
            " auf einen Betrag von " + endkapitalPar + " bei einem Zinssatz von " +
            zinssatzPar + " anwächst,");
        System.out.println("benötigt man " + anzahlLaufjahreLok + " Jahre.");
        return anzahlLaufjahreLok;
    }

    public double zinsenVonJudas(){ // Wie groß ist das verzinste Kapital von Judas?
        double zinssatzLok = 0.01;
        double kapitalLok = 30;
        // i ist einesogenannte Laufvariable für die Wiederholung
        for(int i = 30; i < 2021 ; i++){ // Anzahl der Jahre 2021 - 30 = 1991
            kapitalLok = kapitalLok * (1 + zinssatzLok);
        } 
        return kapitalLok;// Ab hier vergisst der Computer zinssatzLok und kapitalLok
        // -> Lokale Variablen
        // sind nach einer methode ungültig (im Gegensatz zu Attributen / globalen Variablen)
    }

    public int aufgabe2(int aPar, int bPar){
        while(aPar >=       bPar){
            aPar = aPar - bPar;
        }
        return aPar;
    }

    public int aufgabe3(int aPar , int bPar){
        int anzahlWiederholungenLok = 0;
        while(aPar > bPar){
            aPar = aPar - bPar;
            anzahlWiederholungenLok++;// Bei jedem Durchlauf wird die lokale Variable um 1 erhöht
        }
        return anzahlWiederholungenLok++;
    }

    public int aufgabe4(int aPar, int bPar){
        // Statt wie in der Aufgabe verlangt, wird statt a und b hier
        // aPar und bPar benutzt, da diese Parameter sind.
        // Bestimmung des kleinsten gemeinsamen Vielfachen!
        while(aPar != bPar){
            if(aPar > bPar){
                aPar = aPar - bPar;
            }
            else{
                bPar = bPar - aPar;
            }
        } 
        return aPar;
    }

    public void aufgabe5(double kreditPar, double zinssatzPar, double jahresratePar){
        double restschuldLok = kreditPar;
        int tilgungsDauerLok = 0;
        while(jahresratePar < restschuldLok){
            restschuldLok = restschuldLok * ( 1+ zinssatzPar) - jahresratePar; 
            tilgungsDauerLok++;
            System.err.println("Die aktuelle Restschuld beträgt: " + restschuldLok);
        }
        System.out.println("Die Dauer der Tilgung beträgt: " + tilgungsDauerLok + " Jahre.");
        System.out.println("Die Restschuld beträgt: " + restschuldLok);       
    }
    // Methoden Ende
}
