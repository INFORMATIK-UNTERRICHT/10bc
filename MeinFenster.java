import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 14.04.2021
 * @author 
 */

public class MeinFenster extends JFrame {
  // Anfang Attribute
  private JButton bDrueckmich = new JButton();
  private JLabel jLabel1 = new JLabel();
  private JButton bDrueckliebermich = new JButton();
  // Ende Attribute
  
  public MeinFenster() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 600; 
    int frameHeight = 313;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Jetzt geht's los");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    bDrueckmich.setBounds(32, 8, 514, 50);
    bDrueckmich.setText("Dr�ck mich");
    bDrueckmich.setMargin(new Insets(2, 2, 2, 2));
    bDrueckmich.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        bDrueckmich_ActionPerformed(evt);
      }
    });
    bDrueckmich.setFont(new Font("Dialog", Font.BOLD, 40));
    cp.add(bDrueckmich);
    jLabel1.setBounds(32, 96, 505, 57);
    jLabel1.setText("");
    cp.add(jLabel1);
    bDrueckliebermich.setBounds(24, 176, 521, 57);
    bDrueckliebermich.setText("Dr�ck lieber mich");
    bDrueckliebermich.setMargin(new Insets(2, 2, 2, 2));
    bDrueckliebermich.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        bDrueckliebermich_ActionPerformed(evt);
      }
    });
    bDrueckliebermich.setFont(new Font("Dialog", Font.BOLD, 24));
    cp.add(bDrueckliebermich);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public MeinFenster
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new MeinFenster();
  } // end of main
  
  public void bDrueckmich_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
    System.out.println("HALLO WELT");
    this.jLabel1.setText("Button1 wurde gedr�ckt");
  } // end of bDrueckmich_ActionPerformed

  public void bDrueckliebermich_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
    this.jLabel1.setText("Button2 wurde gedr�ckt!");
    System.out.println("HALLO UNTERWELT");
  } // end of bDrueckliebermich_ActionPerformed

  // Ende Methoden
} // end of class MeinFenster

