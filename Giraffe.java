public class Giraffe implements Tier{
    // Attribute
    private int lebensenergie;
    private int lebensalter;
    protected String geschlecht;
    protected String fellfarbe;
    // Attribute Ende
    // ------------------------------
    // Konstruktoren 
    // Standardkonstruktor
    public Giraffe(){
        this.lebensenergie=75;
        this.lebensalter=0;
        this.fellfarbe="gelb";
        this.geschlecht = "männlich";
    }
    
    // Konstruktor mit String Parameter
    public Giraffe(String geschlechtPar){
        this.lebensenergie=75;
        this.lebensalter=0;
        this.fellfarbe="gelb";
        this.geschlecht = geschlechtPar;
    }
    
    // Konstruktoren Ende
    // Methoden
    public void setLebensenergie(int lebensenergiePar) {
        this.lebensenergie = lebensenergiePar;
    }

    public int getLebensenergie() {
        return lebensenergie;
    }

    public int getLebensalter() {
        return this.lebensalter;
    }

    public String getGeschlecht() {
        return this.geschlecht;
    }

    public String getFellfarbe() {
        return this.fellfarbe;
    }

    // Die folgenden Methoden muss es geben,
    // da sonst das Interface Tier nicht implementiert werden kann
    public void gibInformationenAus() {
        System.out.println("Lebensenergie: " + this.lebensenergie);
        System.err.println("Lebensalter: " + this.lebensalter);;
        System.out.println("Fellfarbe: " + this.fellfarbe);
        System.out.println("Geschlecht: " + this.geschlecht);
    }

    public void fresse() {
        this.setLebensenergie(lebensenergie + 5);
    } 

    // Methoden Ende
}