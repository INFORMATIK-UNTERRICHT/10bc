public class GiraffeKind extends Giraffe{
    // Attribute
    private int groesse;
    // Attribute Ende
    // ------------------------------
    // Konstruktoren 
    public GiraffeKind(){
        super("divers");
        this.groesse = 50;
        this.fellfarbe = "gruen";
    }

    public GiraffeKind(int groessePar){
        this.groesse = groessePar;
        this.fellfarbe = "gruen";
    }    

    public GiraffeKind(int groessePar, String geschlechtPar){
        // Erzeugung des inneren Elternobjektes mit Hilfe eines Nichtstandard
        // Konstruktors der Elternklasse
        super(geschlechtPar);
        this.groesse = groessePar;
        this.fellfarbe = "gruen";
    } 
    // Konstruktoren Ende
    // Methoden
    public int getGroesse(){
        return this.groesse;
    }    
    // Methoden Ende    
}
