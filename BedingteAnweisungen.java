// Kommentar eingefügt in BLueJ
// Kommentar eingefügt im Javaeditor
public class BedingteAnweisungen{
    // Attribute
    // Es werden keine Attribute in der Klasse benÃ¶tigt
    // Attribute Ende
    //----------------------------------------
    // Konstruktoren
    public BedingteAnweisungen(){
        // Hier passiert gar nix
    }   
    // Konstruktoren Ende
    //----------------------------------------
    // Methoden
    public double aufgabe1(double rechnungssummePar){
        // Hier wird gegebenenfalls mit Kommazahlen gerechnet (d.h. mit Cent), 
        // daher benÃ¶tigen wir einen neuen Datentyp. Statt int fÃ¼r Ganzzahlen
        // nun double fÃ¼r Kommazahlen.
        // Es wird zudem eine lokale Variable vom Datentyp double benÃ¶tigt
        double endbetragLok = rechnungssummePar;
        if(rechnungssummePar < 300){
            // Hier steht der Quellcode der ausgefÃ¼hrt wird,
            // wenn die Bedingung erfÃ¼llt ist.
            // D.h. wenn die Rechnungsumme kleiner als 300 Euro betrÃ¤gt
            endbetragLok = endbetragLok * 0.98;
            // Durch die Multiplikation mit 0,98 wird vom Betrag 2% abgezogen
            System.out.println("Der Endbetrag betrÃ¤gt: " + endbetragLok);
            // Durch das return wird die Methode verlassen und alle lokalen Variablen
            // und Parameter gelÃ¶scht
            return endbetragLok;
        }
        else{
            // Hier steht der Quellcode der ausgefÃ¼hrt wird,
            // wenn die Bedingung NICHT erfÃ¼llt ist.
            // D.h. wenn die Rechnungsumme grÃ¶ÃŸer oder gleich 300 Euro betrÃ¤gt
            endbetragLok = endbetragLok * 0.95;
            // Durch die Multiplikation mit 0,95 wird vom Betrag 5% abgezogen
            System.out.println("Der Endbetrag betrÃ¤gt: " + endbetragLok);
            return endbetragLok;
        }
    }   

    public int aufgabe2(int a, int b, int c){
        // EinfÃ¼hrung einer lokalen Variable d vom Datentyp int
        // Lokale Variablen werden deklariert und initialisiert (d.h. erhalten Datentyp und Wert in einem Schritt)
        int d = 0;
        if(a>b){
            // Quellcode falls Bedingung erfÃ¼llt, also a grÃ¶ÃŸer b
            d = a;
            // In lokale Variable d wird der Wert von a Ã¼bertragen;
        }
        else{
            // Quellcode falls Bedingung NICHT erfÃ¼llt, also a kleiner (oder gleich) b
            d = b;
            // In lokale Variable d wird der Wert von b Ã¼bertragen;
        }

        if(c>d){
            // Bedingung erfÃ¼llt, wenn Wert von c grÃ¶ÃŸer als der von d
            d = c;
        }
        // Hier gibt es keine Alternative. Es passiert also NICHTS,
        // wenn Bedingung NICHT erfÃ¼llt

        // Ausgabe auf Konsole
        System.out.println("Die grÃ¶ÃŸte Zahl von " + a + " und " + b + " und " + c + " ist: " + d);

        // RÃ¼ckgabe als Wert
        return d;
    }

    public void aufgabe3(double aPar, double bPar ,double cPar){
        double loesungx1Lok,loesungx2Lok;
        double diskriminanteLok = bPar *bPar -4 * aPar * cPar ;
        if( aPar == 0){
            System.out.println("Es handelt sich nicht um eine quadratischen Term.");
        }
        else{
            if (diskriminanteLok > 0) {
                loesungx1Lok=(-bPar + Math.sqrt(diskriminanteLok))/(2*aPar);
                loesungx2Lok=(-bPar - Math.sqrt(diskriminanteLok))/(2*aPar);
                System.out.println("LÃ¶sungen sind: " + loesungx1Lok +" und "+ loesungx2Lok +"}");
            }else 
            if (diskriminanteLok == 0){
                loesungx1Lok =-bPar/(2 * aPar);
                System.out.println("Die einzeige LÃ¶sung lautet:" + loesungx1Lok + ". ");
            }
            else{
                System.out.println("Keine LÃ¶sung vorhanden.");
            }
        }
    }

    public String aufgabe4(int massePar, int groessePar){
        // Als RÃ¼ckgabetyp wird hier String gewÃ¤hlt, da als RÃ¼ckgabe ein
        // String zurÃ¼ckgegeben wird.
        // Beim Berechnen des bmi kann es zu einer Kommazahl kommen,
        // daher muss hier als Datentyp double gewÃ¤hlt werden
        double bmiLok = (massePar/ (groessePar * groessePar));
        if(bmiLok <= 19){
            return "Es liegt Untergewicht vor";
        }
        else{
            if(bmiLok <= 24){
                // Sobald ein return erreicjht wird, wird nach diesem
                // Befehl die Methode verlassen, egal was und wieviel
                // Quellcode danach noch kommt.
                return "Es liegt Normalgewicht vor";
            }
            else{
                if(bmiLok <= 30){
                    return "Es liegt Ãœbergewicht vor";
                }
                else{
                    if(bmiLok <= 40){
                        return "Es liegt Adiopsitas vor";
                    }
                    else{
                        return "Es liegt schwere Adioptias vor";
                    }
                }
            }
        }
    }

    public boolean aufgabe5(int jahreszahlPar){
        int restLok = jahreszahlPar % 400;
        if( restLok == 0){
            // Das ist wahr, wenn die Jahreszahl durch 400 teilbar ist,
            // also der Rest bezÃ¼glich einer Division mit 400 dem Wert
            // 0 entspricht.
            return false;
        }
        else{
            // jahreszahlPar ist also NICHT durch 400 teilbar.
            // Ist sie vielleicht durch 100 teilbar?
            restLok = jahreszahlPar % 100; 
            // Sie ist durch 100 teilbahr, wenn der Rest 0 ist.
            if ( restLok == 0){
                // Falls ja, dann KEIN Schaltjahr
                return false;
            }
            else{
                // Bisher ist die Jahreszahl weder durch 400 noch durch 100 teilbar.
                // Aber vielleicht durch 4?
                restLok = jahreszahlPar % 4; 
                if(restLok == 0){
                    // jahreszahlPar ist durch 4 teilbar, also Schlaltjahr
                    return true;
                }
                else{
                    // jahreszahlPar ist NICHT durch 4 teilbar, also KEIN Schlaltjahr
                    return false;
                }            
            }
        }
    } 
    // Methoden Ende
    //----------------------------------------
}
