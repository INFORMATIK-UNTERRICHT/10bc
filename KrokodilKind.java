public class KrokodilKind extends Krokodil{
    // Attribute
    protected int anzahlSeerosenAlsMakeUp;
    // Attribute Ende
    // ------------------------------
    // Konstruktoren 
    public KrokodilKind(){
        super("divers");
        this.anzahlSeerosenAlsMakeUp = 1;
    }
    // Konstruktoren Ende
    // Methoden
    public int getAnzahlSeerosenAlsMakeUp(){
        return this.anzahlSeerosenAlsMakeUp;
    }    
    // Methoden Ende    
}
