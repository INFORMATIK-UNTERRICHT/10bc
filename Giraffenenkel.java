public class Giraffenenkel extends GiraffeKind{
    // Attribute
    private int anzahlKuscheltiere;
    // Attribute Ende
    // ---------------------------
    // Konstruktoren
    public Giraffenenkel(){
        super();
        this.anzahlKuscheltiere = 1;
    }
    // Konstruktoren Ende
    // ------------------------------
    // Methoden
    public void setAnzahlKuscheltiere(int anzahlKuscheltierePar){
        this.anzahlKuscheltiere = anzahlKuscheltierePar;
    }

    public int getAnzahlKuscheltiere(){
        return this.anzahlKuscheltiere;
    }
    // Methoden Ende
}
